package com.codecream.concepts;

import android.util.Log;
import android.view.View;

public class MyClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        Log.i("listener", "button clicked");
    }
}
